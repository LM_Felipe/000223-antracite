---------------------Etapa 01: Infrared ---------------------

Infra para PLC J: Conv_ToInfraRed
PLC J para Infra: BLACKOUT_IR_UNSOL.GPMCDMSG1Data[0]

Apagar:
	PRT4572
	PRT4571
	PRT4562
	PRT4561

Editar:
(Legenda) Pasta -> Rotina

MainProgram -> MainRoutine
Network 11


MainProgram -> Bypass_Control
	Network 2
	Network 7

	Apagar Network 9
	Apagar Network 8
	Apagar Network 4
	Apagar Network 3

Verificar o que aciona:
	Conv_ToInfraRed.PRB4571.SpareBit12
	Conv_ToInfraRed.PRB4571.SpareBit14

	Conv_ToInfraRed.PRB4572.SpareBit12
	Conv_ToInfraRed.PRB4572.SpareBit14

MainProgram -> Fifo_Control
	Network 1
	Network 5
	Network 8
	Network 9

	Apagar Network 7
	Apagar Network 6
	Apagar Network 3
	Apagar Network 2


MainProgram -> Graph_Control
	Network 3
	
	Apagar Network 18
	Apagar Network 17
	Apagar Network 14
	Apagar Network 13
	Apagar Network 9
	Apagar Network 8
	Apagar Network 6
	Apagar Network 5
	Apagar Network 


OVEN_PRE_HEAT -> MainRoutine
	Network 1

PRB4551 -> MainRoutine
	Network 3

PRB4551 -> Temperature_Control
	Network 5

PRB4552 -> MainRoutine
	Network 3

WRITE_FROM_PLC_J -> MainRoutine
	Apagar Network 31
	Apagar Network 30
	Apagar Network 29
	Apagar Network 28
	Apagar Network 27
	Apagar Network 26
	Apagar Network 25
	Apagar Network 24
	Apagar Network 15
	Apagar Network 14
	Apagar Network 13
	Apagar Network 12
	Apagar Network 11
	Apagar Network 10
	Apagar Network 9
	Apagar Network 8

WRITE_FROM_PLC_J -> PSPI_Data
	Network 0
	Network 2
	
	Apagar Network 47
	Apagar Network 46
	Apagar Network 43
	Apagar Network 42
	Apagar Network 39
	Apagar Network 38
	Apagar Network 37
	Apagar Network 36
	Apagar Network 35
	Apagar Network 34
	Apagar Network 33
	Apagar Network 32
	Apagar Network 23
	Apagar Network 22
	Apagar Network 21
	Apagar Network 20
	Apagar Network 19
	Apagar Network 18
	Apagar Network 17
	Apagar Network 16
	
*Verificar
	GPMCDMSG1Data[40]
	TEMP_TREND_RSVIEW[23]
	Obter backup do PLC 124.2.243.102 -> � servidor do IT N�o � PLC

---------------------Etapa 02: Blackout ---------------------

Informa��es gerais:
	*Parada de processo do BlackSticker est� na linha 9 da PRB4650
	*L�gica de barreira de luz est� na linha 17 da PRB4650
	*Controle de velocidade e flattop est� na PRB4650
	*_010_COMM_AVI Network 20 tem um trigger de start de leitura no PLC do AVI

Criar tags globais:
	START_PRLB4600_1	ok
	CONF_START_PRLB4600_1	ok
	PRB4648_PX1		ok
	START_PRT4647		ok
	CONF_START_PRT4647	ok
	PRB4647_PX3_ATFRONT 	ok

PRLB4600_1 -> zV110_PRB_AB284VFD
	Network 9	ok
		

PRLB4600_1 -> B001_MapInputs
	*Delay_ClearToEnter -> N�o � utilizado
	Network 7 da rotina PRB4649, copiar para aqui ?
	Se sim na linha acima, adicionar branch 2 da linha 09 da rotina PRB4649
	

PRLB4600_1 -> B002_MapOutputs
	*Linha 2 n�o faz nada
	*Linha 3 n�o faz nada
	Network 5	tok

PRLB4600_1 -> B020_Setup
	*UnitPRB.EnableHSFwd da linha 10 passar para false ? (prb4649 � false, olhar c�digo de l�)
	*Na prb4649 h� o UnitPRB.EnableSyncFwd (linha 10) que n�o tem na PRLB4600_1, colocar ?
	*Network 13 na prb4649 o brake release tamb�m olha a mesa da frente, fazer igual ?

PRLB4600_1 -> zS021_SkidIndexIn
	*Network 4, adicionar SPI_FlapTop_Enable ?

PRLB4600_1 -> zS021_SkidIndexOut
	Criar tag local: HABILITA_STOPPER
	Network 2
	Network 3
	*Network 4, adicionar SPI_FlapTop_Enable ?
	*Network 5, adicionar PRTs_BlackSticker_Avancar ?
	*Network 6, copiar l�gica da PRB4649 ?
	
PRLB4600_1 -> zB100_Interlocks_PRBA
	Network 19

PRB4647 -> zV110_PRB_AB284VFD
	Network 9	ok
	Network 15	ok

PRB4647 -> B001_MapInputs
	Network 4
	Apagar Network 5
	*Olhar l�gica 6 da PRB4650
	*Parada de processo do BlackSticker est� na linha 9 da PRB4650
	*L�gica de barreira de luz est� na linha 17 da PRB4650

PRB4647 -> B002_MapOutputs
	Network 1
	*Controle de velocidade e flattop est� na PRB4650
	
PRB4647 -> B020_Setup
	Network 7
	Network 10
	
PRB4647 -> zS021_SkidIndexIn
	Network 2
	Network 4
	Network 5

PRB4647 -> zS021_SkidIndexOut
	Network 2
	Network 4
	Network 5
	Network 6

PRB4648 -> B002_MapInputs
	Network 4

PRB4648 -> B002_MapOutputs
	Apagar Network 4
	Apagar Network 3
	Apagar Network 2
	Network 2
	
PRB4648 -> B020_Setup
	Network 9
	Network 11
	Network 21
	Network 32
	Network 33

PRB4648 -> zS021_SkidIndexIn
	Network 2
	Network 3
	Network 4
	Network 5

PRB4648 -> zS021_SkidIndexOut
	Network 2
	Network 3
	Network 4
	Network 5
	
PRB4649 -> B001_MapInputs
	Apagar network 5
	
PRB4649 -> zB100_Interlocks_PRBA
	Network 03
	Network 19
	Network 20
	Network 27
	
PRB4649 -> zS021_SkidIndexIn
	Network 2
	Network 3
	Network 5
	
PRB4649 -> zS021_SkidIndexOut
	Network 2
	
---------------------Etapa 03: UR'S ----------------------	
Criar tags globais:

	PRT4648_CarData				ok
	PRT4647_CarData				ok

	BC_CONVERTIDO_0_ANO_PLANTA_4648		ok
	BC_CONVERTIDO_0_ANO_PLANTA_4647		ok

	BC_CONVERTIDO_1_PVI_1_4648		ok
	BC_CONVERTIDO_1_PVI_1_4647		ok

	BC_CONVERTIDO_2_PVI_2_4648		ok
	BC_CONVERTIDO_2_PVI_2_4647		ok

	BC_CONVERTIDO_3_MOD_VER_4648		ok
	BC_CONVERTIDO_3_MOD_VER_4647		ok

	BC_CONVERTIDO_4_COR_OP_4648		ok
	BC_CONVERTIDO_4_COR_OP_4647		ok

	PRB4648_Delay_Position_Filter -> Setar para true !!!!	ok

	PRB4647_Delay_Position_Filter		ok

	PRB4647_Delay_Position			ok

	ONS_4600_TO_4647			modificado ONS_4600_TO_4649

	BC_CONVERTIDO_1_PVI_1_PRT4647		ok
	BC_CONVERTIDO_2_PVI_2_PRT4647		ok
	BC_CONVERTIDO_3_MOD_VER_PRT4647		ok
	BC_CONVERTIDO_4_COR_OP_PRT4647		ok

	ONS_4648_TO_4649			ok
	ONS_4647_TO_4648			ok

	
PLC_W2 -> _106_UR129	
	Network 6	ok
	Network 8	ok
	
_010_COMM_AVI Network 20 tem um trigger de start de leitura no PLC do AVI
	
Part_Picking -> B040_Data_Stack_Styles
	Network 2	ok
	Network 3	ok
	Network 4	ok
	Network 5	ok
	
RFID_6_RCO129 -> x022_PRT_TRACKING_RFID_R6 ok
	Network 2	ok
	Network 3	ok
	Network 4	ok
	Network 5	ok
	Network 8	ok
	Network 9	ok
	Network 21	ok
	Network 24	ok
	
BC_BLACKOUT -> B999_BarCode_BC_BLACKOUT
	Network 2	ok
	Network 23	ok
	Network 24	ok
	Network 37	ok
	Network 38	ok
	Network 39	ok
	Network 40	ok
	Network 42	ok
	Network 43	ok	

BC_BLACKOUT -> SR_1000
	Network 0		ok
	Network 5		ok
	Network 6		ok
	Adicionar Network 7	ok
	Adicionar Network 8	ok
	Adicionar Network 9	ok
	Adicionar Network 10	ok

--------------- UR127 --------------------- ok
	
RFID_4_RCO127 -> x022_PRT_TRACKING_RFID_R4
	Network 9	ok
	Network 24	ok
	
PRB4614 -> B020_Setup (4634 !!!)
	Network 5	ok

--------------- UR128 --------------------- ok

RFID_5_RCO128 -> x022_PRT_TRACKING_RFID_R5
	Network 2	ok
	Network 3	ok
	Network 4	ok
	Network 5	ok
	Network 9	ok
	Network 21	ok
	Network 24	ok
	Network 26	ok

	

	
	
	
	